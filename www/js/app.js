// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova'])

.run(function($ionicPlatform, $ionicPopup, $cordovaSQLite,$cordovaNetwork,$cordovaToast,$timeout, $rootScope, $cordovaNetwork, $ionicPopup, $http) {
  $ionicPlatform.ready(function() {
    console.log();
    
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
   
     $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      var sinh = {};
      sinh.placanje = JSON.parse(localStorage.getItem('localPlacanje'));
      sinh.studenti = JSON.parse(localStorage.getItem('localStudent'));
      sinh.treneri = JSON.parse(localStorage.getItem('localTrener'));
      sinh.grupe = JSON.parse(localStorage.getItem('localGrupa'));
      console.log(sinh);
      if (!(sinh.placanje == null && sinh.studenti == null && sinh.treneri == null && sinh.grupe == null)) {
        $http.post("http://metdevstudio.com/CDT/insertAll.php",sinh).then(function(response){
              localStorage.setItem('localPlacanje',JSON.stringify(null));
              localStorage.setItem('localStudent',JSON.stringify(null));
              localStorage.setItem('localGrupa',JSON.stringify(null));
              localStorage.setItem('localTrener',JSON.stringify(null));
        });
      }
       $http.post("http://metdevstudio.com/CDT/getAll.php").then(function(response){
            console.log(response.data);
            $rootScope.treneri = response.data.treneri;
            $rootScope.studenti = response.data.studenti;
            $rootScope.grupe = response.data.grupe;
            $rootScope.clan_grupe = response.data.clan_grupe;
            $rootScope.placanja = response.data.placanja;
            localStorage.setItem('treneri',JSON.stringify(response.data.treneri));
            localStorage.setItem('studenti',JSON.stringify(response.data.studenti));
            localStorage.setItem('grupe',JSON.stringify(response.data.grupe));
            localStorage.setItem('clan_grupe',JSON.stringify(response.data.clan_grupe));
            localStorage.setItem('placanja',JSON.stringify(response.data.placanja));
        });

      $ionicPopup.confirm({
                        title: "Sinhronizacija",
                        content: ""
                    });
    });

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('sviStudentiGrupe', {
    url: '/sviStudentiGrupe',
   
        templateUrl: 'templates/sviStudentiGrupe.html', 
        controller: 'SviStudentiGrupe'
  })

    .state('sms', {
    url: '/sms',
   
        templateUrl: 'templates/sms.html', 
        controller: 'SmsCtrl'
  })

     .state('login', {
    url: '/login',
   
        templateUrl: 'templates/login.html', 
        controller: 'LoginCtrl'
  })
     .state('info', {
    url: '/info',
   
        templateUrl: 'templates/info.html'
  })
    .state('dash', {
    url: '/dash',
   
        templateUrl: 'templates/dash.html', 
        controller: 'DashCtrl'
  })
    .state('kreirajStudenta', {
      url: '/kreirajStudenta',
          templateUrl: 'templates/kreirajStudenta.html',
          controller: 'KreirajStudentaCtrl'
    })

.state('kreirajGrupu', {
      url: '/kreirajGrupu',
          templateUrl: 'templates/kreirajGrupu.html',
          controller: 'KreirajGrupuCtrl' 
    })

.state('kreirajTrenera', {
      url: '/kreirajTrenera',
          templateUrl: 'templates/kreirajTrenera.html',
          controller: 'KreirajTreneraCtrl'
    })
.state('kreirajPlacanje', {
      url: '/kreirajPlacanje',
          templateUrl: 'templates/kreirajPlacanje.html',
          controller: 'KreirajPlacanjeCtrl'
    })

.state('svaPlacanja', {
      url: '/svaPlacanja',
          templateUrl: 'templates/svaPlacanja.html',
          controller: 'SvaPlacanjaCtrl'
    })
.state('rokoviZaPlacanje', {
      url: '/rokoviZaPlacanje/:rok',
          templateUrl: 'templates/rokoviZaPlacanje.html',
          controller: 'RokoviZaPlacanjeCtrl'
    })

.state('studentiZaRok', {
      url: '/studentiZaRok/:placanje/:rok',
          templateUrl: 'templates/studentiZaRok.html',
          controller: 'StudentiZaRokCtrl'
    })

.state('student', {
      url: '/student/:idStudent',
          templateUrl: 'templates/student.html',
          controller: 'StudentCtrl'
    })
.state('skenirajKod', {
      url: '/skenirajKod',
          templateUrl: 'templates/skenirajKod.html',
          controller: 'SkenirajKodCtrl'
    })

.state('studenti', {
      url: '/studenti',
          templateUrl: 'templates/studenti.html',
          controller: 'StudentiCtrl'
    })

.state('sveGrupe', {
      url: '/sveGrupe',
          templateUrl: 'templates/sveGrupe.html',
          controller: 'SveGrupeCtrl'
    })

.state('grupa', {
      url: '/grupa/:idGrupa',
          templateUrl: 'templates/grupa.html',
          controller: 'GrupaCtrl'
    })

.state('treneri', {
      url: '/treneri',
          templateUrl: 'templates/treneri.html',
          controller: 'TreneriCtrl'
    })

.state('trener', {
      url: '/trener/:idTrener',
          templateUrl: 'templates/trener.html',
          controller: 'TreneriCtrl'
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
