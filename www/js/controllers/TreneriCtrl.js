angular.module('starter.controllers')
.controller('TreneriCtrl', function($scope, $http,$stateParams,GetAll) {
	GetAll.start();
  var treneri = JSON.parse(localStorage.getItem('treneri'));
  for (var i = 0; i < treneri.length; i++) {
  	if(treneri[i].id == $stateParams.idTrener){
  		$scope.trener = treneri[i]; 
  	}
  }
});