angular.module('starter.controllers')
.controller('SmsCtrl', function($scope, $cordovaSms){


  document.addEventListener("deviceready", function () {
  $scope.posaljiSMS = function(data) {
    
        var options = {
        replaceLineBreaks: false, // true to replace \n by a new line, false by default
        android: {
          intent: '' // send SMS with the native android SMS messaging
            //intent: '' // send SMS without open any other app
            //intent: 'INTENT' // send SMS inside a default SMS app
        }
      };
      $cordovaSms
        .send(data.brojTelefona, data.sadrzajPoruke, options)
        .then(function() {
          console.log("uspelo");
        }, function(error) {
          console.log(error);
        });

  
  }
  }); // body...

}); 