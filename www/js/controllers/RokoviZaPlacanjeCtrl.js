angular.module('starter.controllers')
.controller('RokoviZaPlacanjeCtrl', function($scope, $http,$stateParams,GetAll) {
	GetAll.start();
	var i = $stateParams.rok;
  var placanja = JSON.parse(localStorage.getItem('placanja'));
  $scope.rokovi = placanja[i].rokovi;
  $scope.placanjeId = placanja[i].id;

});