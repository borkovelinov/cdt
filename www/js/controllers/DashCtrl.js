angular.module('starter.controllers')
.controller('DashCtrl', function($scope,$cordovaBarcodeScanner , $location, $http, $ionicPopup,$rootScope,GetAll){
if(JSON.parse(localStorage.getItem('admin')) == null){
  $location.path('/login'); 
}else{  
$scope.admin = JSON.parse(localStorage.getItem('admin'));
console.log(JSON.parse(localStorage.getItem('admin')))
 GetAll.start();

  $scope.kamera = function (){
    $ionicPopup.show({
    title: 'QR Kod',
    scope: $scope,
    buttons: [
      { 
        text: 'Manuelno',
        onTap: function(e) {
           $ionicPopup.prompt({
               title: 'Manuelni unos',
               inputType: 'text',
               inputPlaceholder: 'Unesite clanski broj'
             }).then(function(res) {
                 $location.path('/student/'+res); 
               
             });

        }
      },
      
      {
        text: '<b>Ocitaj QR Kod</b>',
        type: 'button-positive',
        onTap: function(e) {
           
     $cordovaBarcodeScanner
      .scan()
      .then(function(barcodeData) {
        
          $location.path('/student/'+barcodeData.text); 
            

      }, function(error) {
        // An error occurred
      });
         
        }
      }
    ]
  });
 
     
 };
}
});