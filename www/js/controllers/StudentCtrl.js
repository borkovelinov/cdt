angular.module('starter.controllers')
.controller('StudentCtrl', function($scope,$http, $ionicPopup,$cordovaNetwork,$stateParams,$cordovaBarcodeScanner, $cordovaSms, $rootScope, $cordovaNetwork,$location,GetAll){
  GetAll.start();

  var res = $stateParams.idStudent;
  var studenti = JSON.parse(localStorage.getItem('studenti'));
  var id = 0;
  for (var i = 0; i < studenti.length; i++) {
    console.log(res == studenti[i].id);
    if (res == studenti[i].id) {
      id = i;           
      break;
    }else if(i==studenti.length-1){
      alert("Student ne postoji");
      $location.path('#/dash'); 
      
    };              
  };
  
  console.log(studenti[id]);
  $scope.ime = studenti[id].ime;
  $scope.id = studenti[id].id;
  $scope.placanja = studenti[id].placanja;
  $scope.prezime = studenti[id].prezime;
  $scope.imeRoditelja = studenti[id].imeRoditelja;
  $scope.broj = studenti[id].broj;
  $scope.jmbg = studenti[id].jmbg;
  $scope.brojRoditelja = studenti[id].brojRoditelja;
  $scope.ulica = studenti[id].ulica;
  document.addEventListener("deviceready", function () {
  $scope.posaljiSMS = function () {
    $ionicPopup.prompt({
               title: 'Unesi poruku',
               inputType: 'text',
               inputPlaceholder: 'poruka'
             }).then(function(res) {
                  var options = {
                    replaceLineBreaks: false, // true to replace \n by a new line, false by default
                    android: {
                      intent: '' // send SMS with the native android SMS messaging
                        //intent: '' // send SMS without open any other app
                        //intent: 'INTENT' // send SMS inside a default SMS app
                    }
                  };
                  var br;
                  if ($scope.brojRoditelja == "" || $scope.brojRoditelja ==null) {
                    br = $scope.broj;
                  }else{
                    br = $scope.brojRoditelja;
                  }
                  $cordovaSms
                    .send(br, res, options)
                    .then(function() {
                      $ionicPopup.confirm({
                       title: 'SMS',
                       template: 'SMS poslat!'
                     });
                    }, function(error) {
                      $ionicPopup.confirm({
                       title: 'SMS',
                       template: 'SMS nije poslat!'
                     });
                    });
               
             });
  };
  });
  $scope.plati = function (){
      if(window.Connection){
        if(navigator.connection.type != Connection.NONE){
          var data = {id :$scope.id };
          $http.post("http://metdevstudio.com/CDT/platio.php",data).then(function(response){
            $cordovaBarcodeScanner
      .scan()
      .then(function(barcodeData) {
        var studenti = JSON.parse(localStorage.getItem('studenti'));
        var ids;
        for (var i = 0; i < studenti.length; i++) {
          if (barcodeData.text == studenti[i].id) {
            alert("student "+i);
             $location.path('/student/'+i);
             break;
          }else if(i==studenti.length-1){
            alert("Student ne postoji");
          };
        };

      }, function(error) {
        // An error occurred
      });
          });
        
      }

    }
};
$scope.prisutan = function  () {
  if(window.Connection){
        if(navigator.connection.type != Connection.NONE){
          $http.post("http://metdevstudio.com/CDT/platio.php").then(function(response){
            
            $cordovaBarcodeScanner
      .scan()
      .then(function(barcodeData) {
        var studenti = JSON.parse(localStorage.getItem('studenti'));
        var ids;
        for (var i = 0; i < studenti.length; i++) {
          if (barcodeData.text == studenti[i].id) {
            alert("student "+i);
             $location.path('/student/'+i);
             break;
          }else if(i==studenti.length-1){
            alert("Student ne postoji");
          };
        };

      }, function(error) {
        // An error occurred
      });
          });
        
      }

    }
};

});