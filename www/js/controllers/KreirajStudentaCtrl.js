angular.module('starter.controllers')
.controller('KreirajStudentaCtrl', function($scope,$http,$ionicPopup,$cordovaNetwork,$rootScope,$cordovaToast,$cordovaBarcodeScanner,GetAll, $location) {
 GetAll.start().then(function(response){
    console.log(response)
 });
$scope.studenti = JSON.parse(localStorage.getItem('studenti'));
var grupe = JSON.parse(localStorage.getItem('grupe'));
  $scope.grupe = grupe;
  if (grupe.length < GetAll.getGrupe().length) {
    $scope.grupe = GetAll.getGrupe();

  }
  console.log($scope.grupe)
var sinh = JSON.parse(localStorage.getItem('localStudent'));
console.log(sinh);
 $scope.reset = function(){
    $('#ime').val("");
    $('#prezime').val("");
    $('#imeRoditelja').val("");
    $('#broj').val("");
    $('#jmbg').val("");
    $('#brojRoditelja').val("");
    $('#ulica').val("");
    $('#grupa').val("");
 };
  $scope.submit = function (){
      var student = {};
     if((/\d/.test($("#ime").val()))|| $("#ime").val() == "" || $("#ime").val()== null) {
        $ionicPopup.alert({
     title: 'GRESKA!',
     template: 'Lose uneto ime!'
   });
    }else if((/\d/.test($("#prezime").val()))|| $("#prezime").val() == "" || $("#prezime").val()== null){
      $ionicPopup.alert({
     title: 'GRESKA!',
     template: 'Lose uneto prezime!'
   });
    
    }else if($("#jmbg").val().length !=13 || $("#jmbg").val()=="" || $("#jmbg").val()==null){
      $ionicPopup.alert({
     title: 'GRESKA!',
     template: 'Lose unet jmbg!'
   });
    }else{
     
      if(window.Connection){
        if(navigator.connection.type != Connection.NONE){
          
          $http.post("http://metdevstudio.com/CDT/insertStudent.php?ime="+$("#ime").val()+"&prezime="+$("#prezime").val()+"&ulica="+$("#ulica").val()+"&jmbg="+$("#jmbg").val()+"&broj="+$("#broj").val()+"&imeRoditelja="+$("#imeRoditelja").val()+"&brojRoditelja="+$("#brojRoditelja").val()).then(function(response){
            $ionicPopup.confirm({
                        title: "Cuvanje podataka",
                        content: response.data.msg
                    });
            
          });
        
      }else{
          student.ime = $("#ime").val();
          student.prezime = $("#prezime").val();
          student.ulica = $("#ulica").val();
          student.jmbg = $("#jmbg").val();
          student.broj = $("#broj").val();
          student.imeRoditelja = $("#imeRoditelja").val();
          student.brojRoditelja = $("#brojRoditelja").val();
          console.log(student);

        if (sinh == null) {
          sinh = [];
          sinh.push(student);
          localStorage.setItem('localStudent',JSON.stringify(sinh));
        }else{
          sinh.push(student);
          localStorage.setItem('localStudent',JSON.stringify(sinh));
        }
        $ionicPopup.confirm({
                        title: "Cuvanje podataka",
                        content: "Sacuvano u lokalu"
                    });
        //OVDE U LOKAL CUVAS
      }
    }
    }
     $location.path('/dash'); 
  };
});