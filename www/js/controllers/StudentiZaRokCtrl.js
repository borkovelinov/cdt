angular.module('starter.controllers')
.controller('StudentiZaRokCtrl', function($scope, $http,$stateParams,$ionicPopup,GetAll, $cordovaSms) {
  GetAll.start();
	var placanja = JSON.parse(localStorage.getItem('placanja'));
	var studenti = JSON.parse(localStorage.getItem('studenti'));
	var placanje = $stateParams.placanje;
	var rok = $stateParams.rok;
	$scope.studenti = [];
	var br = "";
	angular.forEach(studenti,function(student,key){

		angular.forEach(student.placanja,function(value,key){
				if(value.id == placanje && value.datum == rok){
					student.platio = value.platio;
					if(student.platio == 0 && (student.brojRoditelja == "" || student.brojRoditelja == null)){
						br += student.broj+ ",";	
					}else if(student.platio == 0){
            br += student.brojRoditelja+ ",";
          }
					
					$scope.studenti.push(student);
			}
		});
	});
	$scope.posaljiSMS = function () {
		console.log(br);
    $ionicPopup.prompt({
               title: 'Unesi poruku',
               inputType: 'text',
               inputPlaceholder: 'poruka'
             }).then(function(res) {
                  var options = {
                    replaceLineBreaks: false, // true to replace \n by a new line, false by default
                    android: {
                      intent: '' // send SMS with the native android SMS messaging
                        //intent: '' // send SMS without open any other app
                        //intent: 'INTENT' // send SMS inside a default SMS app
                    }
                  };
                  $cordovaSms
                    .send($scope.brojRoditelja, res, options)
                    .then(function() {
                      $ionicPopup.confirm({
                       title: 'SMS',
                       template: 'SMS poslat!'
                     });
                    }, function(error) {
                      $ionicPopup.confirm({
                       title: 'SMS',
                       template: 'SMS nije poslat!'
                     });
                    });
               
             });
  };
  // $scope.studenti = JSON.parse(localStorage.getItem('studenti'));
  // $scope.rokovi = JSON.parse(localStorage.getItem('rokovi'));
  // $scope.placanja = JSON.parse(localStorage.getItem('placanja'));

});