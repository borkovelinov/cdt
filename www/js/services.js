angular.module('starter.services', [])

.service('GetAll', function($http,$rootScope) {
    var studenti;
    var grupe;
    var clan_grupe;
    var placanja;
  this.start = function (){

    // if(window.Connection){
    //     if(navigator.connection.type != Connection.NONE){
          return $http.post("http://metdevstudio.com/CDT/getAll.php").then(function(response){
            
            $rootScope.treneri = response.data.treneri;
            console.log($rootScope.treneri);
            studenti = response.data.studenti;
            grupe = response.data.grupe;
            console.log(grupe);
            clan_grupe = response.data.clan_grupe;
            placanja = response.data.placanja;
            localStorage.setItem('treneri',JSON.stringify(response.data.treneri));
            localStorage.setItem('studenti',JSON.stringify(response.data.studenti));
            localStorage.setItem('grupe',JSON.stringify(response.data.grupe));
            localStorage.setItem('clan_grupe',JSON.stringify(response.data.clan_grupe));
            localStorage.setItem('placanja',JSON.stringify(response.data.placanja));
              return response;
        });
   //     }else{
   //      return null;
   //     }
   // }

  }
  this.getStudenti= function(){
    return studenti;
  }
  this.getGrupe= function(){
    console.log(grupe);
    return grupe;
  }
  this.getPlacanja= function(){
    return placanja;
  }
  this.getTreneri= function(){
    return treneri;
  }
});
